use super::Filter;
use crate::template::Template;
use ffcli::{FFmpegArgs, VideoFilter, VideoFilterParams};
use serde::Deserialize;

#[serde(rename_all = "lowercase")]
#[derive(Deserialize, Debug)]
pub enum EvaluateOn {
    Init,
    Frame,
}

#[serde(rename_all = "snake_case")]
#[derive(Deserialize, Debug)]
pub enum Interlacing {
    Force,
    SourceAware,
    DoNotApply,
}
#[serde(rename_all = "lowercase")]
#[derive(Deserialize, Debug)]
pub enum ForceOriginal {
    Disable,
    Decrease,
    Increase,
}

#[derive(Deserialize, Debug)]
pub struct Scale {
    #[serde(skip_serializing_if = "Option::is_none")]
    width: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    height: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    evaluate_on: Option<EvaluateOn>,
    #[serde(skip_serializing_if = "Option::is_none")]
    interlacing: Option<Interlacing>,
    #[serde(skip_serializing_if = "Option::is_none")]
    force_original_aspect_ratio: Option<ForceOriginal>,
    #[serde(skip_serializing_if = "Option::is_none")]
    size: Option<String>,
}

impl Filter for Scale {
    fn apply(
        &self,
        args: FFmpegArgs,
        _template: &Template,
        input_video: &mut String,
        _input_audio: &mut String,
    ) -> FFmpegArgs {
        let mut args = args;

        let mut video_params = VideoFilterParams::new().key("scale");

        if let Some(width) = self.width.as_ref() {
            video_params = video_params.params_raw(VideoFilterParams::kv("width", width));
        }

        if let Some(height) = self.height.as_ref() {
            video_params = video_params.params_raw(VideoFilterParams::kv("height", height));
        }

        if let Some(size) = self.size.as_ref() {
            video_params = video_params.params_raw(VideoFilterParams::kv("size", size));
        }

        if let Some(evaluate_on) = self.evaluate_on.as_ref() {
            video_params = video_params.params_raw(VideoFilterParams::kv(
                "eval",
                match evaluate_on {
                    EvaluateOn::Frame => "frame",
                    EvaluateOn::Init => "init",
                },
            ));
        }

        if let Some(interlacing) = self.interlacing.as_ref() {
            video_params = video_params.params_raw(VideoFilterParams::kv(
                "interlacing",
                match interlacing {
                    Interlacing::Force => 1,
                    Interlacing::DoNotApply => 0,
                    Interlacing::SourceAware => -1,
                },
            ));
        }

        if let Some(force_original) = self.force_original_aspect_ratio.as_ref() {
            video_params = video_params.params_raw(VideoFilterParams::kv(
                "force_original_aspect_ratio",
                match force_original {
                    ForceOriginal::Decrease => "decrease",
                    ForceOriginal::Disable => "disable",
                    ForceOriginal::Increase => "increase",
                },
            ))
        }

        let output_video = format!("s{}", input_video.clone());

        let video_scale = VideoFilter::new()
            .input(input_video.clone())
            .output(output_video.clone())
            .params_raw(video_params);
        args = args.vf(video_scale);

        *input_video = output_video;

        args
    }
}
