use super::Filter;
use crate::template::Template;
use ffcli::{FFmpegArgs, VideoFilter, VideoFilterParams};
use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct Zoompan {
    #[serde(skip_serializing_if = "Option::is_none")]
    zoom: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    x: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    y: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    duration: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    size: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    framerate: Option<i32>,
}

impl Filter for Zoompan {
    fn apply(
        &self,
        args: FFmpegArgs,
        _template: &Template,
        input_video: &mut String,
        _input_audio: &mut String,
    ) -> FFmpegArgs {
        let mut args = args;

        let mut video_params = VideoFilterParams::new().key("zoompan");

        if let Some(duration) = self.duration.as_ref() {
            video_params =
                video_params.params_raw(VideoFilterParams::kv("d", duration.clone()));
        }

        if let Some(zoom) = self.zoom.as_ref() {
            video_params =
                video_params.params_raw(VideoFilterParams::kv("zoom", zoom.clone()));
        }
        
        if let Some(x) = self.x.as_ref() {
            video_params =
                video_params.params_raw(VideoFilterParams::kv("x", x.clone()));
        }
        
        if let Some(y) = self.y.as_ref() {
            video_params =
                video_params.params_raw(VideoFilterParams::kv("y", y.clone()));
        }

        if let Some(size) = self.size.as_ref() {
            video_params =
                video_params.params_raw(VideoFilterParams::kv("s", size.clone()));
        }
        
        if let Some(framerate) = self.framerate.as_ref() {
            video_params =
                video_params.params_raw(VideoFilterParams::kv("fps", framerate));
        }

        let output_video = format!("z{}", input_video.clone());

        let video_zoom = VideoFilter::new()
            .input(input_video.clone())
            .output(output_video.clone())
            .params_raw(video_params);

        args = args.vf(video_zoom);

        *input_video = output_video;

        args
    }
}
