mod filters;
mod template;

use ffcli::{FFmpegArgs, VideoFilter};
use filters::Filter;
use log::Level;
use std::{
    env,
    fs::{canonicalize, read_to_string},
    io::{Error, ErrorKind},
    process::Command,
};
use template::Template;

fn main() {
    let mut args = env::args().skip(1);

    let template_path = match args
        .next()
        .ok_or_else(|| Error::new(ErrorKind::Other, "No arg"))
        .and_then(canonicalize)
    {
        Ok(path) => path,
        Err(_) => {
            eprintln!("Error: no template specified.");
            return;
        }
    };

    let out_path = match args.next().ok_or("No arg") {
        Ok(path) => path,
        Err(err) => {
            eprintln!("Error: no output path specified.");
            eprintln!("{}", err);
            return;
        }
    };

    let template = match read_to_string(template_path) {
        Ok(template) => template,
        Err(err) => {
            eprintln!("Error: can't read template contents.");
            eprintln!("{}", err);
            return;
        }
    };

    let template: Template = match toml::from_str(template.as_str()) {
        Ok(template) => template,
        Err(err) => {
            eprintln!("Error: invalid template format.");
            eprintln!("{}", err);
            return;
        }
    };

    let mut args = FFmpegArgs::new(Level::Debug);
    let mut concat_filter = VideoFilter::new()
        .params("concat", format!("n={}:a=1", template.videos.len()))
        .output("out");

    for video in template.videos.iter() {
        let mut full_path = std::env::current_dir().unwrap().to_path_buf();
        full_path.push(video.path.as_str());
        args = args.i(full_path.display());
    }

    for (id, video) in template.videos.iter().enumerate() {
        let mut input_video = format!("{}:v", id);
        let mut input_audio = format!("{}:a", id);

        if let Some(framerate) = &video.framerate {
            let input_video_o = input_video.clone();
            input_video = format!("f{}", input_video.clone());
            args = args.vf(VideoFilter::new()
                .input(input_video_o)
                .output(input_video.clone())
                .params("fps", format!("fps={}", framerate)));
        }

        if let Some(trim) = &video.trim {
            args = trim.apply(args, &template, &mut input_video, &mut input_audio);
        }

        if let Some(zoom) = &video.zoompan {
            args = zoom.apply(args, &template, &mut input_video, &mut input_audio);
        }

        if let Some(matrix) = &video.matrix {
            args = matrix.apply(args, &template, &mut input_video, &mut input_audio);
        }

        if let Some(scale) = &video.scale {
            args = scale.apply(args, &template, &mut input_video, &mut input_audio);
        }

        if let Some(fade) = &video.fade {
            args = fade.apply(args, &template, &mut input_video, &mut input_audio);
        }

        if let Some(geq) = &video.generic_equation {
            let input_video_o = input_video.clone();
            input_video = format!("g{}", input_video.clone());
            args = args.vf(VideoFilter::new()
                .input(input_video_o)
                .output(input_video.clone())
                .params("geq", geq));
        }

        concat_filter = concat_filter.input(input_video).input(input_audio);
    }

    args = args.vf(concat_filter).build_filter();

    
    let args = args.map("[out]").raw(out_path).build();
    
    println!("{}", args.join(" "));

    let mut child = match Command::new("ffmpeg").args(args).spawn() {
        Ok(child) => child,
        Err(err) => {
            eprintln!("Error: can't run fmmpeg.");
            eprintln!("{}", err);
            return;
        }
    };

    match child.wait() {
        Ok(code) => {
            println!("Ffmpeg exited with code {}", code);
            println!("Completed!");
        }
        Err(err) => {
            eprintln!("Error: cant process template.");
            eprintln!("{}", err);
        }
    }
}
