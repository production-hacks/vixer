use crate::template::Template;
use ffcli::FFmpegArgs;

mod fade;
mod scale;
mod trim;
mod zoompan;
mod matrix;

pub use fade::Fade;
pub use scale::Scale;
pub use trim::Trim;
pub use zoompan::Zoompan;
pub use matrix::Matrix;

pub trait Filter {
    fn apply(
        &self,
        args: FFmpegArgs,
        template: &Template,
        input_video: &mut String,
        input_audio: &mut String,
    ) -> FFmpegArgs;
}
