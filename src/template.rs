use crate::filters::{Fade, Scale, Trim, Zoompan, Matrix};
use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct Template {
    pub videos: Vec<Video>,
}

#[derive(Deserialize, Debug)]
pub struct Video {
    pub path: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub trim: Option<Trim>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub scale: Option<Scale>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fade: Option<Fade>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub zoompan: Option<Zoompan>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub framerate: Option<i32>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub generic_equation: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub matrix: Option<Matrix>,
}
