use super::Filter;
use crate::template::Template;
use ffcli::{FFmpegArgs, VideoFilter, VideoFilterParams};
use serde::Deserialize;

#[serde(rename_all = "lowercase")]
#[derive(Deserialize, Debug)]
enum Direction {
    In,
    Out,
}
#[derive(Deserialize, Debug)]
pub struct Fade {
    #[serde(skip_serializing_if = "Option::is_none")]
    direction: Option<Direction>,
    #[serde(skip_serializing_if = "Option::is_none")]
    alpha_only: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    start: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    duration: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    color: Option<String>,
}

impl Filter for Fade {
    fn apply(
        &self,
        args: FFmpegArgs,
        _template: &Template,
        input_video: &mut String,
        _input_audio: &mut String,
    ) -> FFmpegArgs {
        let mut args = args;

        let mut video_params = VideoFilterParams::new().key("fade");

        if let Some(direction) = self.direction.as_ref() {
            video_params = video_params.params_raw(VideoFilterParams::kv(
                "type",
                match direction {
                    Direction::In => "in",
                    Direction::Out => "out",
                },
            ));
        }

        if let Some(alpha_only) = self.alpha_only {
            video_params = video_params.params_raw(VideoFilterParams::kv(
                "alpha",
                if alpha_only { 1 } else { 0 },
            ));
        }

        if let Some(start) = self.start.as_ref() {
            video_params = video_params.params_raw(VideoFilterParams::kv("start_time", start));
        }

        if let Some(duration) = self.duration.as_ref() {
            video_params = video_params.params_raw(VideoFilterParams::kv("duration", duration));
        }

        if let Some(color) = self.color.as_ref() {
            video_params = video_params.params_raw(VideoFilterParams::kv("color", color));
        }

        let output_video = format!("f{}", input_video.clone());

        let video_scale = VideoFilter::new()
            .input(input_video.clone())
            .output(output_video.clone())
            .params_raw(video_params);
        args = args.vf(video_scale);

        *input_video = output_video;

        args
    }
}
