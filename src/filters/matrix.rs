use super::Filter;
use crate::template::Template;
use ffcli::{FFmpegArgs, VideoFilter};
use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct Matrix {
    rows: usize,
    columns: usize,
}

impl Filter for Matrix {
    fn apply(
        &self,
        args: FFmpegArgs,
        _template: &Template,
        input_video: &mut String,
        _input_audio: &mut String,
    ) -> FFmpegArgs {
        let mut column_filter = VideoFilter::new().params("hstack", self.columns);
        let mut column_split_filter = VideoFilter::new()
            .params("split", self.columns)
            .input(input_video.clone());

        for column in 0..self.columns {
            column_filter = column_filter.input(format!("sp{}{}", column, input_video.clone()));
            column_split_filter =
                column_split_filter.output(format!("sp{}{}", column, input_video.clone()))
        }
        *input_video = format!("hs{}", input_video);
        column_filter = column_filter.output(input_video.clone());

        let mut rows_filter = VideoFilter::new().params("vstack", self.rows);
        let mut row_split_filter = VideoFilter::new()
            .params("split", self.rows)
            .input(input_video.clone());

        for row in 0..self.rows {
            rows_filter = rows_filter.input(format!("sp{}{}", row, input_video.clone()));
            row_split_filter = row_split_filter.output(format!("sp{}{}", row, input_video.clone()));
        }

        *input_video = format!("hv{}", input_video);
        rows_filter = rows_filter.output(input_video.clone());

        args.vf(column_split_filter)
            .vf(column_filter)
            .vf(row_split_filter)
            .vf(rows_filter)
    }
}
