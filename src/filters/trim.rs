use super::Filter;
use crate::template::Template;
use ffcli::{FFmpegArgs, VideoFilter, VideoFilterParams};
use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct Trim {
    #[serde(skip_serializing_if = "Option::is_none")]
    start: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    end: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    duration: Option<String>,
}

impl Filter for Trim {
    fn apply(
        &self,
        args: FFmpegArgs,
        _template: &Template,
        input_video: &mut String,
        input_audio: &mut String,
    ) -> FFmpegArgs {
        let mut args = args;

        let mut video_params = VideoFilterParams::new().key("trim");
        let mut audio_params = VideoFilterParams::new().key("atrim");

        if let Some(duration) = self.duration.as_ref() {
            let duration = duration.replace(":", "\\:");
            video_params =
                video_params.params_raw(VideoFilterParams::kv("duration", duration.clone()));
            audio_params = audio_params.params_raw(VideoFilterParams::kv("duration", duration));
        }

        if let Some(start) = self.start.as_ref() {
            let start = start.replace(":", "\\:");
            video_params = video_params.params_raw(VideoFilterParams::kv("start", start.clone()));
            audio_params = audio_params.params_raw(VideoFilterParams::kv("start", start));
        }

        if let Some(end) = self.end.as_ref() {
            let end = end.replace(":", "\\:");
            video_params = video_params.params_raw(VideoFilterParams::kv("end", end.clone()));
            audio_params = audio_params.params_raw(VideoFilterParams::kv("end", end));
        }

        let output_video = format!("t{}", input_video.clone());
        let output_audio = format!("t{}", input_audio.clone());

        let video_trim = VideoFilter::new()
            .input(input_video.clone())
            .output(output_video.clone())
            .params_raw(video_params)
            .params("setpts", "'PTS-STARTPTS'");

        let audio_trim = VideoFilter::new()
            .input(input_audio.clone())
            .output(output_audio.clone())
            .params_raw(audio_params)
            .params("asetpts", "'PTS-STARTPTS'");

        args = args.vf(video_trim);
        args = args.vf(audio_trim);

        *input_video = output_video;
        *input_audio = output_audio;

        args
    }
}
